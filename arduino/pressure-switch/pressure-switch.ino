
#include <TM1637Display.h>
#include "config.h"
#include "encoder.h"

#define ZERO_BAR_VOLTAGE    440
#define MAX_BAR_VOLTAGE     1750
#define MAX_BAR             3600
#define RELAY               7

TM1637Display display(6, 5);
Encoder knob(2, 3, 4);

void setup() {
  Serial.begin(115200);
  restoreConfig();
  pinMode(RELAY, OUTPUT);
  display.setBrightness(1);
  knob.begin();
  knob.setStep(5);
}

void loop()
{
  static unsigned long t;
  long encoderValue;
  static long actualEncoder;

  if (knob.readButton() == 0) {
    Serial.println("configuration");
    actualEncoder = configData.minPressure;
    knob.write(actualEncoder);
    while (knob.readButton() != 0) {
      encoderValue = knob.read();
      display.showNumberDecEx(encoderValue, 0b10000000, true);
      if (encoderValue != actualEncoder) {
        Serial.print("value: "); Serial.print(encoderValue); Serial.println(" mbar");
        display.showNumberDecEx(encoderValue, 0b10000000, true);
        actualEncoder = encoderValue;
      }
    }
    configData.minPressure = encoderValue;
    actualEncoder = configData.maxPressure;
    knob.write(actualEncoder);
    while (knob.readButton() != 0) {
      encoderValue = knob.read();
      display.showNumberDecEx(encoderValue, 0b10000000, true);
      if (encoderValue != actualEncoder) {
        Serial.print("value: "); Serial.print(encoderValue); Serial.println(" mbar");
        display.showNumberDecEx(encoderValue, 0b10000000, true);
        actualEncoder = encoderValue;
      }
    }
    configData.maxPressure = encoderValue;
    saveConfig();
  }
  float voltage = analogRead(A0) * (5.06 / 1023.0) * 1000;
  float bar = map(voltage, ZERO_BAR_VOLTAGE, MAX_BAR_VOLTAGE, 0, MAX_BAR);
  if (millis() - t > 1000) {
    Serial.print("Volts: "); Serial.print(voltage); Serial.println(" mV");
    Serial.print("Bar: "); Serial.print(bar); Serial.println(" mbar");
    display.showNumberDecEx(bar, 0b10000000, true);
    t = millis();
  }
  if (bar < configData.minPressure) {
    if (digitalRead(RELAY) == LOW) {
      Serial.println("RELAY ON");
    }
    digitalWrite(RELAY, HIGH);
  }
  if (bar > configData.maxPressure) {
    if (digitalRead(RELAY) == HIGH) {
      Serial.println("RELAY OFF");
    }
    digitalWrite(RELAY, LOW);
  }
}
