
#include <Arduino.h>
#include <EEPROM.h>

#include "config.h"

struct config_data configData;

void saveConfig(void)
{
  configData.magic = MAGIC;
  EEPROM.put(CONFIG_ADDR, configData);
}

void restoreConfig(void)
{
  EEPROM.get(CONFIG_ADDR, configData);
  if (configData.magic != MAGIC) {
    Serial.println("no config available in EEPROM");
    resetConfig();
  }
  else {
    Serial.println("valid config data has been found in the EEPROM: ");
    Serial.print("magic: "); Serial.println(configData.magic, HEX);
    Serial.print("min pressure: "); Serial.println(configData.minPressure);
    Serial.print("max pressure: "); Serial.println(configData.maxPressure);
  }
}

void resetConfig(void)
{
  Serial.println("put default config in EEPROM");
  configData.minPressure = MIN_PRESSURE;
  configData.maxPressure = MAX_PRESSURE;
  saveConfig();
}
