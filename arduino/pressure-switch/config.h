
#ifndef _CONFIG_H_
#define _CONFIG_H_

#define MIN_PRESSURE        1200
#define MAX_PRESSURE        1600
#define MAGIC               0xDEADBEEF
#define CONFIG_ADDR         0

struct config_data
{
  int minPressure;
  int maxPressure;
  unsigned long magic;
};

void saveConfig(void);
void restoreConfig(void);
void resetConfig(void);

extern struct config_data configData;

#endif
